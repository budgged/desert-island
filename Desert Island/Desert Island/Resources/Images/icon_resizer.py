import os
import re
import shutil
import Image

DEBUG = True
ROOT_PATH = os.path.dirname(os.path.abspath(__file__))

for r,d,f in os.walk(ROOT_PATH):
  for files in f:
    path = os.path.join(r,files)
    if (re.search(".*-ipadhd.*",path)):
      retinaImagePath = path;
      retinaImage = Image.open(retinaImagePath)
      os.remove(retinaImagePath)
      retinaImage.save(retinaImagePath)

      if (DEBUG == True):
        print path

      nonRetinaImagePath = retinaImagePath.replace("-ipadhd","-ipad")
      iphImagePath = retinaImagePath.replace("-ipadhd","-hd")
      iphNonRetinaImagePath = retinaImagePath.replace("-ipadhd","")
      if  not (os.path.isfile(nonRetinaImagePath)):
        nonRetinaImage = Image.open(retinaImagePath)
        nonRetinaImage.thumbnail([nonRetinaImage.size[0] / 2, nonRetinaImage.size[1] / 2])
        nonRetinaImage.save(nonRetinaImagePath)
        if (DEBUG == True):
          print("Now adding ",nonRetinaImagePath);

      if  not (os.path.isfile(iphImagePath)):
        nonRetinaImage = Image.open(retinaImagePath)
        nonRetinaImage.thumbnail([int(nonRetinaImage.size[0] / 2.4), int(nonRetinaImage.size[1] / 2.4)])
        nonRetinaImage.save(iphImagePath)
        if (DEBUG == True):
          print("Now adding ",iphImagePath);

      if  not (os.path.isfile(iphNonRetinaImagePath)):
        nonRetinaImage = Image.open(retinaImagePath)
        nonRetinaImage.thumbnail([int(nonRetinaImage.size[0] / 4.8), int(nonRetinaImage.size[1] / 4.8)])
        nonRetinaImage.save(iphNonRetinaImagePath)
        if (DEBUG == True):
          print("Now adding ",iphNonRetinaImagePath);