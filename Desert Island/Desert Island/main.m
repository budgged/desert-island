//
//  main.m
//  Desert Island
//
//  Created by Anton Siliuk on 07.12.13.
//  Copyright Part Two 2013. All rights reserved.
//

#import <UIKit/UIKit.h>

int main(int argc, char *argv[]) {
    
    @autoreleasepool {
        int retVal = UIApplicationMain(argc, argv, nil, @"AppController");
        return retVal;
    }
}
