//
//  DIScoreCounter.h
//  Desert Island
//
//  Created by Anton Siliuk on 09.12.13.
//  Copyright (c) 2013 Part Two. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DICathcingObject.h"

@protocol DIScoreCatcherDelegate
- (void)scoreHasChanged:(NSInteger)newScore;
@end

@interface DIScoreCounter : NSObject

@property (readonly, nonatomic) NSInteger score;
@property (weak, nonatomic) id<DIScoreCatcherDelegate> delegate;

- (void)registerMiss;
- (void)objectWasCathced:(DICathcingObject*)catchedObject;

@end
