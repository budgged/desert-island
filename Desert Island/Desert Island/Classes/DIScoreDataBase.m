//
//  DIScoreDataBase.m
//  Desert Island
//
//  Created by Anton Siliuk on 10.12.13.
//  Copyright (c) 2013 Part Two. All rights reserved.
//

#import "DIScoreDataBase.h"

@implementation DIScoreDataBase

+ (NSUserDefaults *)getDefaults
{
    return [NSUserDefaults standardUserDefaults];
}

@end
