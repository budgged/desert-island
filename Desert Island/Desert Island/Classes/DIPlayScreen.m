//
//  DIPlayScreen.m
//  Desert Island
//
//  Created by Anton Siliuk on 07.12.13.
//  Copyright (c) 2013 Part Two. All rights reserved.
//

#import "DIPlayScreen.h"
#import "DIScoreCounter.h"
#import "DIBox.h"
#import "DICathcingObject.h"
#import "DIScoreDataBase.h"
#import <UIKit/UIAlertView.h>
#import <Foundation/Foundation.h>

@interface DIPlayScreen() <DIScoreCatcherDelegate, UIAlertViewDelegate>

@property (nonatomic) CGSize screenSize;
@property (nonatomic) BOOL isMovingLivebuoy;
@property (strong, nonatomic) CCSprite *lifebuoy;

//------------lifebuoy animatin-----------------

@property (nonatomic) CGPoint startPoint;
@property (nonatomic) ccTime startTime;
@property (nonatomic) CGPoint endPoint;

@property (nonatomic) ccTime currTime;

@property (nonatomic) ccTime stopTime;
@property (nonatomic) CGPoint moveBy;

//---------------game logic-----------------

@property (strong, nonatomic) NSMutableArray *objectsToCatch;
@property (strong, nonatomic) DIScoreCounter *scoreCounter;
@property (strong, nonatomic) CCLabelTTF *scoreLabel;

//---------------timer----------------------

@property (strong, nonatomic) CCLabelTTF *timeLabel;
@property (nonatomic) NSInteger timeLeft;
@end

@implementation DIPlayScreen

#define WAVE_TAG 101
#define WAVE_COUNT 25
#define ARC4RANDOM_MAX 0x100000000
#define WAVE_SPRITES_COUNT 6
#define GRAVITY_COEFF 1000
#define TOP_LAYER 200
#define ROUND_TIME 120

+ (CCScene*)scene
{
    CCScene *scene = [CCScene node];
    DIPlayScreen *layer = [DIPlayScreen node];
    [scene addChild: layer];
    
    return scene;
}


- (id)init
{
    self = [super init];
    if (self) {
        self.screenSize = [[CCDirector sharedDirector] winSize];
        
        CCMenuItem *backButton = [CCMenuItemImage itemWithNormalImage:@"back.png" selectedImage:@"back.png" block:^(id sender) {
            [[CCDirector sharedDirector] popScene];
        }];
        
        CCMenu *menu = [CCMenu menuWithItems:backButton, nil];
        CGSize menuSize = [backButton boundingBox].size;
        [menu setPosition:ccp(self.screenSize.width - menuSize.width, self.screenSize.height - menuSize.height)];
        [self addChild:menu z:TOP_LAYER];
        
        self.currTime = 0;
        self.stopTime = 0;
        
        self.timeLeft = ROUND_TIME;
        [self timerTick];
        
        self.objectsToCatch = [[NSMutableArray alloc] init];
        self.scoreCounter = [[DIScoreCounter alloc] init];
        self.scoreCounter.delegate = self;
        [self scoreHasChanged:0];
        
        self.isMovingLivebuoy = NO;
        
        ccColor4B color = {115,218,240,255};
        CCLayerColor *colorLayer = [CCLayerColor layerWithColor:color];
        [self addChild:colorLayer z:-1];
        

        CCSprite *island = [CCSprite spriteWithFile:@"island.png"];
        island.position = ccp(self.screenSize.width/2, [island boundingBox].size.height/2);
        
        float waveOffset = self.screenSize.height - [island boundingBox].size.height/2;
        waveOffset /= WAVE_COUNT;
        [self createOceanWithWaveOffset:waveOffset];
        
        [self addChild:island z:TOP_LAYER];
        
        self.lifebuoy = [CCSprite spriteWithFile:@"lifebuoy.png"];
        [self.lifebuoy setPosition:ccp(self.screenSize.width/2, [self.lifebuoy boundingBox].size.height/2)];
        [self addChild:self.lifebuoy z:TOP_LAYER];
        
        _touchEnabled = YES;
        
        DIBox *testBox = [[DIBox alloc] init];
        [self placeItemInOcean:testBox inPosition:ccp(self.screenSize.width/2, self.screenSize.height/2)];
        
        [self scheduleUpdate];
        [self schedule:@selector(timerTick) interval:1.0];
    }
    return self;
}

- (void)timerTick
{
    if (!self.timeLabel) {
        self.timeLabel = [CCLabelTTF labelWithString:@"" fontName:@"Arial" fontSize:24];
        self.timeLabel.color = ccBLACK;
        [self addChild:self.timeLabel z:TOP_LAYER];
    }
    if (!self.scoreLabel) {
        [self scoreHasChanged:0];
    }
    NSInteger minutesLeft = self.timeLeft / 60;
    NSInteger secondsLeft = self.timeLeft % 60;
    [self.timeLabel setString:[NSString stringWithFormat:@"Left: %d:%d",minutesLeft,secondsLeft]];
    CGSize scoreLabelSize = [self.scoreLabel boundingBox].size;
    CGSize timeLabelSize = [self.timeLabel boundingBox].size;
    
    [self.timeLabel setPosition:ccp(timeLabelSize.width, self.screenSize.height - scoreLabelSize.height - timeLabelSize.height)];
    if (self.timeLeft > 0) {
        self.timeLeft--;
    } else {
        [self unschedule:@selector(timerTick)];
        [self gameHasEnded];
    }
}

//======================================TOUCH EVENTS===========================
#define START_ANIMATION_DURATION 0.2
- (void)startAgain
{
    [self.lifebuoy stopAllActions];
    
    id scaleDown = [CCScaleBy actionWithDuration:START_ANIMATION_DURATION/2 scale:1/4.0];
    id scaleUp = [CCScaleBy actionWithDuration:START_ANIMATION_DURATION/2 scale:4];
    id rotate = [CCRotateBy actionWithDuration:START_ANIMATION_DURATION angle:360];
    id move = [CCMoveTo actionWithDuration:0 position:ccp(self.screenSize.width/2, [self.lifebuoy boundingBox].size.height/2)];
    
    id actions = [CCSequence actions:[CCSpawn actions:scaleDown, rotate, nil], [CCSpawn actions:move, rotate, scaleUp, nil], nil];
    [self.lifebuoy runAction:actions];
    
    
    
    self.isMovingLivebuoy = NO;
    self.stopTime = 0;
    self.moveBy = ccp(0, 0);
}

- (void)ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    CGPoint touchPos = [self convertTouchToNodeSpace:touch];
    CGPoint lifebuoyPos = self.lifebuoy.position;
    CGFloat distance = powf(touchPos.x - lifebuoyPos.x, 2) +
        powf(touchPos.y - lifebuoyPos.y, 2);
    distance = sqrtf(distance);
    
    if (distance <= [self.lifebuoy boundingBox].size.width / 2) {
        self.startPoint = touchPos;
        self.startTime = self.currTime;
        self.isMovingLivebuoy = YES;
    } else {
        [self startAgain];
    }
}

- (void)ccTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    self.endPoint = [self convertTouchToNodeSpace:[touches anyObject]];
    if (self.isMovingLivebuoy) {
        CGPoint translation = ccpSub(self.endPoint, self.startPoint);
        CGFloat speedY = ccpLength(translation)/sqrtf(2);
        
        self.stopTime = 2 * (speedY / GRAVITY_COEFF);
        ccTime delta = self.currTime - self.startTime;
        
        CGPoint velocity = ccp(translation.x / delta, translation.y / delta);
        velocity = ccpMult(velocity, 1/sqrtf(2.0));
        
        self.moveBy = ccpMult(velocity, self.stopTime);
        CGFloat scaleFactor =1 + 0.4*ccpLength(self.moveBy)/self.screenSize.height;
        
        id scaleUp = [CCScaleBy actionWithDuration:self.stopTime/2 scale:scaleFactor];
        id scaleDown = [CCScaleBy actionWithDuration:self.stopTime/2 scale:1/scaleFactor];
        id lifebuoyHasStoped = [CCCallFunc actionWithTarget:self selector:@selector(lifebuoyHasStoped)];

        [self.lifebuoy runAction:[CCSequence actions:scaleUp, scaleDown, lifebuoyHasStoped, nil]];
    }
    self.isMovingLivebuoy = NO;
}

//====================================ANIMATION===============================

- (void)placeItemInOcean:(DICathcingObject *)catchingObject inPosition:(CGPoint)posToPlace
{
    int zToPalce = TOP_LAYER;
    catchingObject.position = posToPlace;
    for (id child in self.children) {
        if ([child isKindOfClass:[CCSprite class]]) {
            if ([(CCSprite*)child tag] == WAVE_TAG) {
                CCSprite *wave = child;
                if (CGRectContainsPoint(wave.boundingBox, posToPlace) && zToPalce > [wave zOrder]) {
                    zToPalce = [wave zOrder];
                }
            }
        }
    }
    zToPalce++;
    id swingAnimation = [CCSequence actions:
                         [CCRotateBy actionWithDuration:0.5 angle:30],
                         [CCRotateBy actionWithDuration:0.5 angle:-30]
                         , nil];
    [catchingObject.sprite runAction:[CCRepeatForever actionWithAction:swingAnimation]];
    [self addChild:catchingObject.sprite z:zToPalce];
    [self.objectsToCatch addObject:catchingObject];
}
//---------------------------------------OCEAN AND WAVES------------------------------

- (void)createOceanWithWaveOffset:(float)waveOffset
{
    CGPoint position = ccp(self.screenSize.width / 2, self.screenSize.height);
    for (int i = 0; i < WAVE_COUNT; i++) {
        CCSprite *wave = [CCSprite spriteWithFile:[NSString stringWithFormat:@"wave%d.png",i % WAVE_SPRITES_COUNT + 1]];

        wave.tag = WAVE_TAG;
        
        CGPoint wavePosition = position;
        [wave setPosition:wavePosition];
        //[wave setAnchorPoint:wavePosition];
        
        [self addChild:wave z:i];
        position.y -= waveOffset;
    }
    [self moveWaves];
}

- (void)moveWaves
{
    for (id child in [self children]){
        if ([child isKindOfClass:[CCSprite class]] && [(CCSprite*)child tag] == WAVE_TAG){
            ccTime timeInterval = (double)arc4random() / ARC4RANDOM_MAX + 0.5;
            CGFloat movingOffset = [child boundingBox].size.width - self.screenSize.width;
            movingOffset /= 2;
            id animationUP = [CCMoveBy actionWithDuration:timeInterval position:ccp(movingOffset,0)];
            id animationDOWN = [CCMoveBy actionWithDuration:timeInterval position:ccp(-movingOffset,0)];
            id action = [CCSequence actions:animationUP, animationDOWN, nil];
            [child runAction:[CCRepeatForever actionWithAction:action]];
            
        }
    }
}

//---------------------------------------SCHEDULED METHOD------------------------------

- (void) update:(ccTime)delta {
    self.currTime += delta;
    if (!self.isMovingLivebuoy){
        //move lifebuoy if needed
        if (self.stopTime >= 0) {
            [self.lifebuoy runAction:[CCMoveBy actionWithDuration:delta position:ccpMult(self.moveBy, delta)]];
            self.stopTime -= delta;
        }
        
        //reflect vertical
        if (self.lifebuoy.position.y < 0 || self.lifebuoy.position.y > self.screenSize.height){
            self.moveBy = ccp(self.moveBy.x,-self.moveBy.y);
            CGFloat yToMove = (self.lifebuoy.position.y < 0) ? 0 : self.screenSize.height;
            [self.lifebuoy runAction:[CCMoveTo actionWithDuration:0 position:ccp(self.lifebuoy.position.x, yToMove)]];
        }
        
        //reflect horisontal
        if (self.lifebuoy.position.x < 0 || self.lifebuoy.position.x > self.screenSize.width){
            self.moveBy = ccp(-self.moveBy.x,self.moveBy.y);
            CGFloat xToMove = (self.lifebuoy.position.x < 0) ? 0 : self.screenSize.width;
            [self.lifebuoy runAction:[CCMoveTo actionWithDuration:0 position:ccp(xToMove, self.lifebuoy.position.y)]];
        }
    }
}
//====================================ALERT VIEW DELEGATE===============================

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
        {
            [self resetGame];
            break;
        }
        case 1:
        {
            [[CCDirector sharedDirector] popScene];
            break;
        }
        default:
        {
            assert(@"Uncknown button index");
            break;
        }
    }
}

//====================================GAME LOGIC===============================
- (void)resetGame
{
    self.timeLeft = ROUND_TIME;
    [self timerTick];
    
    [self scoreHasChanged:0];
    
    [self schedule:@selector(timerTick) interval:1.0];
    self.scoreCounter = [[DIScoreCounter alloc] init];
    self.scoreCounter.delegate = self;
    [self startAgain];
}

- (void)gameHasEnded
{
    NSMutableArray *scoreArray = [[[DIScoreDataBase getDefaults] objectForKey:kScoreArray] mutableCopy];
    if (!scoreArray) {
        scoreArray = [[NSMutableArray alloc] init];
    }
    NSString *currUsername = [[DIScoreDataBase getDefaults] objectForKey:kCurrentUsername];
    [scoreArray addObject:@{ kUsername : currUsername,
                             kUserScore : [NSNumber numberWithInt:self.scoreCounter.score]
                             }];
    [[DIScoreDataBase getDefaults] setObject:scoreArray forKey:kScoreArray];
    
    UIAlertView *gameEndingAlert = [[UIAlertView alloc]
                                    initWithTitle:NSLocalizedString(@"Congratulations", @"Alert title")
                                    message:[NSString stringWithFormat:
                                             NSLocalizedString(@"Time has ended. Your score is %d", @"Alert message"), self.scoreCounter.score]
                                    delegate:self
                                    cancelButtonTitle:NSLocalizedString(@"Play again", @"Alert button")
                                    otherButtonTitles:NSLocalizedString(@"Exit", @"Alert button"), nil];
    [gameEndingAlert show];
}

- (void)lifebuoyHasStoped
{
    BOOL miss = YES;
    self.stopTime = 0;
    self.moveBy = ccp(0,0);
    for (DICathcingObject *objToCatch in self.objectsToCatch) {
        if (CGRectContainsPoint([self.lifebuoy boundingBox], objToCatch.position)) {
            miss = NO;
            [self.scoreCounter objectWasCathced:objToCatch];

            [self removeChild:objToCatch.sprite];
            [self.objectsToCatch removeObject:objToCatch];
            DIBox *testBox = [[DIBox alloc] init];
            [self placeItemInOcean:testBox inPosition:ccp(arc4random()% (int)self.screenSize.width, (arc4random() % (int)self.screenSize.height*1/3) + self.screenSize.height/3)];

        }
    }
    if (miss) {
        [self.scoreCounter registerMiss];
    }
    [self startAgain];
}

- (void)scoreHasChanged:(NSInteger)newScore
{
    NSLog(@"Curr score:%d",newScore);
    if (!self.scoreLabel) {
        self.scoreLabel = [CCLabelTTF labelWithString:@"" fontName:@"Arial" fontSize:24];
        self.scoreLabel.color = ccBLACK;
        [self addChild:self.scoreLabel z:TOP_LAYER];
    }
    [self.scoreLabel setString:[NSString stringWithFormat:NSLocalizedString(@"Score: %d",@"Score label"),newScore]];
    CGSize labelSize = [self.scoreLabel boundingBox].size;
    [self.scoreLabel setPosition:ccp(labelSize.width, self.screenSize.height - labelSize.height)];
}
@end
