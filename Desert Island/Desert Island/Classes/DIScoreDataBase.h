//
//  DIScoreDataBase.h
//  Desert Island
//
//  Created by Anton Siliuk on 10.12.13.
//  Copyright (c) 2013 Part Two. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kCurrentUsername @"current username"
#define kScoreArray @"score array"
#define kUsername @"username"
#define kUserScore @"score"

@interface DIScoreDataBase : NSObject

+ (NSUserDefaults*)getDefaults;

@end
