//
//  DICathcingObject.h
//  Desert Island
//
//  Created by Anton Siliuk on 09.12.13.
//  Copyright (c) 2013 Part Two. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface DICathcingObject : NSObject

@property (strong, nonatomic) CCSprite *sprite;
@property (nonatomic) NSInteger scorePrice;
@property (nonatomic) CGPoint position;

@end
