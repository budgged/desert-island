//
//  DIPlayScreen.h
//  Desert Island
//
//  Created by Anton Siliuk on 07.12.13.
//  Copyright (c) 2013 Part Two. All rights reserved.
//

#import "cocos2d.h"

@interface DIPlayScreen : CCLayer

+ (CCScene*)scene;

@end
