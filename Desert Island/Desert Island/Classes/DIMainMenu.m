//
//  DIMainMenu.m
//  Desert Island
//
//  Created by Anton Siliuk on 07.12.13.
//  Copyright (c) 2013 Part Two. All rights reserved.
//

#import "DIMainMenu.h"
#import "DIPlayScreen.h"
#import <UIKit/UIAlertView.h>
#import "DIScoreDataBase.h"
#import "DIScoreTabel.h"

@interface DIMainMenu()<UIAlertViewDelegate>

@end
@implementation DIMainMenu

+ (CCScene*)scene
{
    CCScene *scene = [CCScene node];
	DIMainMenu *layer = [DIMainMenu node];
	[scene addChild: layer];
	
	return scene;
}

#define VERTICAL_PADDING 40
#define FONT_SIZE 35
- (id)init
{
    self = [super init];
    if (self) {
        CCSprite *back = [CCSprite spriteWithFile:@"background.jpg"];
        [back setAnchorPoint:ccp(0,0)];
        [self addChild:back z:-1];
        [CCMenuItemFont setFontSize:FONT_SIZE];
        
        CCMenuItem *itemPlay = [CCMenuItemFont
                                itemWithString:NSLocalizedString(@"Play" , @"Main menu item")
                                target:self
                                selector:@selector(playWasPressed)];
        itemPlay.color = ccBLACK;
        CCMenuItem *itemStatistics = [CCMenuItemFont
                                      itemWithString:NSLocalizedString(@"Statistics", @"Main menu item")
                                      target:self
                                      selector:@selector(statisticsWasPressed)];
        itemStatistics.color = ccBLACK;

        CCMenu *mainMenu = [CCMenu menuWithItems:itemPlay, itemStatistics, nil];
        
        [mainMenu alignItemsVerticallyWithPadding:VERTICAL_PADDING];
        
        CGSize screenSize = [[CCDirector sharedDirector] winSize];
        [mainMenu setPosition:ccp(screenSize.width / 2, screenSize.height / 2)];
        
        [self addChild:mainMenu];
        
    }
    return self;
}

//==========================================MENU REACTION===============================

- (void)playWasPressed
{
    NSLog(@"Need to start new game");
    UIAlertView *userNameAlert = [[UIAlertView alloc]
                                  initWithTitle:NSLocalizedString(@"Username", @"")
                                  message:@"Please enter username"
                                  delegate:self
                                  cancelButtonTitle:NSLocalizedString(@"Done", @"")
                                  otherButtonTitles:nil];
    userNameAlert.alertViewStyle = UIAlertViewStylePlainTextInput;
    userNameAlert.delegate = self;
    [userNameAlert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *username = [alertView textFieldAtIndex:0].text;
    [[DIScoreDataBase getDefaults] setObject:username forKey:kCurrentUsername];
    [[CCDirector sharedDirector] pushScene:[DIPlayScreen scene]];
}

- (void)statisticsWasPressed
{
    [[CCDirector sharedDirector] pushScene:[DIScoreTabel scene]];

}

@end
