//
//  DIBox.m
//  Desert Island
//
//  Created by Anton Siliuk on 09.12.13.
//  Copyright (c) 2013 Part Two. All rights reserved.
//

#import "DIBox.h"

#define BOX_SCORE_PRICE 300
@implementation DIBox
- (id)init
{
    self = [super init];
    if (self) {
        self.sprite = [CCSprite spriteWithFile:@"box.png"];
        self.scorePrice = BOX_SCORE_PRICE;
        self.position = ccp(0,0);
    }
    return self;
}
@end
