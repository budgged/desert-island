//
//  DICathcingObject.m
//  Desert Island
//
//  Created by Anton Siliuk on 09.12.13.
//  Copyright (c) 2013 Part Two. All rights reserved.
//

#import "DICathcingObject.h"

@implementation DICathcingObject

- (void)setPosition:(CGPoint)position
{
    _position = position;
    [self.sprite setPosition:position];
}

@end
