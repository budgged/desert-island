//
//  DIScoreCounter.m
//  Desert Island
//
//  Created by Anton Siliuk on 09.12.13.
//  Copyright (c) 2013 Part Two. All rights reserved.
//

#import "DIScoreCounter.h"

@interface DIScoreCounter()
@property (nonatomic) NSInteger scoreMult;

@end

@implementation DIScoreCounter

- (id)init
{
    self = [super init];
    if (self) {
        _score = 0;
        _scoreMult = 1;
    }
    return self;
}

- (void)registerMiss
{
    self.scoreMult = 1;
}

- (void)objectWasCathced:(DICathcingObject*)catchedObject
{
    _score += catchedObject.scorePrice * self.scoreMult;
    [self.delegate scoreHasChanged:self.score];
    self.scoreMult++;
}

@end
