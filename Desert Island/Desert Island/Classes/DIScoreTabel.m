//
//  DIScoreTabel.m
//  Desert Island
//
//  Created by Anton Siliuk on 10.12.13.
//  Copyright (c) 2013 Part Two. All rights reserved.
//

#import "DIScoreTabel.h"
#import "DIScoreDataBase.h"

@implementation DIScoreTabel

+ (CCScene*)scene
{
    CCScene *scene = [CCScene node];
	DIScoreTabel *layer = [DIScoreTabel node];
	[scene addChild: layer];
	
	return scene;
}

#define RESULT_COUNT 10

- (id)init
{
    self = [super init];
    if (self) {
        CGSize screenSize = [[CCDirector sharedDirector] winSize];


        CCMenuItem *backButton = [CCMenuItemImage itemWithNormalImage:@"back.png" selectedImage:@"back.png" block:^(id sender) {
            [[CCDirector sharedDirector] popScene];
        }];
        
        CCMenu *menu = [CCMenu menuWithItems:backButton, nil];
        CGSize menuSize = [backButton boundingBox].size;
        [menu setPosition:ccp(screenSize.width - menuSize.width, screenSize.height - menuSize.height)];
        [self addChild:menu];

        CCSprite *back = [CCSprite spriteWithFile:@"background.jpg"];
        [back setAnchorPoint:ccp(0,0)];
        [self addChild:back z:-1];

        CCLabelTTF *header = [CCLabelTTF labelWithString:
                              NSLocalizedString(@"Top 10 results:", @"Score tabel header")
                                                fontName:@"Arial" fontSize:30];
        [header setPosition:ccp(screenSize.width/2, screenSize.height - 60)];
        header.color = ccBLACK;
        [self addChild:header];
        
        CGFloat offset = screenSize.height*0.8 / RESULT_COUNT;
        NSArray *scoreArray = [[DIScoreDataBase getDefaults] objectForKey:kScoreArray];
        scoreArray = [scoreArray sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
            NSInteger score1 = [[obj1 valueForKey:kUserScore] integerValue];
            NSInteger score2 = [[obj2 valueForKey:kUserScore] integerValue];
            return score1 < score2;
        }];
        
        for (int i = 0; i < RESULT_COUNT; i++) {
            NSString *line;
            if ([scoreArray count] > i) {
                NSString *username = [[scoreArray objectAtIndex:i] valueForKey:kUsername];
                NSInteger userScore = [[[scoreArray objectAtIndex:i] valueForKey:kUserScore] integerValue];
                line = [NSString stringWithFormat:@"%@ : %d",username,userScore];
            } else {
                line = @"-- : --";
            }
            
            CCLabelTTF *lineLabel = [CCLabelTTF labelWithString:line fontName:@"Arial" fontSize:24];
            lineLabel.color = ccBLACK;

            [lineLabel setPosition:ccp(screenSize.width / 2, screenSize.height*0.8 - i*offset)];
            [self addChild:lineLabel];
        }
    }
    return self;
}

@end
